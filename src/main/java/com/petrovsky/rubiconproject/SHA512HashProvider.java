package com.petrovsky.rubiconproject;

import com.google.common.hash.Hashing;

/**
 * Class provides a hash calculation of incoming bytes by SHA512 algorithm
 * <p>
 * Copyright © 2016 Rubicon Project, All rights reserved.
 */
public class SHA512HashProvider implements HashProvider {

    @Override
    public String getHash(byte[] inputBytes) {

        return Hashing.sha512()
                .hashBytes(inputBytes)
                .toString();
    }
}

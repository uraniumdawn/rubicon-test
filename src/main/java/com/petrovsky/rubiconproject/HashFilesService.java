package com.petrovsky.rubiconproject;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.FileVisitor;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.Arrays;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.*;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Class provides walking files tree, calculate hash of ones content and write result to specified file
 * <p>
 * Copyright © 2016 Rubicon Project, All rights reserved.
 */
public class HashFilesService {
    private static final Logger LOGGER = LogManager.getLogger(HashFilesService.class);

    private static final String FORMAT = "%s %s\n";

    private Map<String, String> container = new ConcurrentHashMap<>();
    private HashProvider hashProvider;
    private Path inputPath;
    private FileReader fileReader;
    private FileWriter fileWriter;

   private HashFilesService(Builder builder) {
      hashProvider = builder.hashProvider;
      inputPath = builder.inputPath;
      fileReader = builder.fileReader;
      fileWriter = builder.fileWriter;
   }

   public static Builder newBuilder()
   {
      return new Builder();
   }

   /**
     * Method provides start walking process
     */
    public void process() throws IOException {

            walk(inputPath);
            Map<String, String> sortedMap = getSortedMap(container);

            fileWriter.createFile();
            fileWriter.openChannel();

            for(Map.Entry entry : sortedMap.entrySet()) {
                String row = String.format(FORMAT, entry.getKey(), entry.getValue());
                fileWriter.write(row.getBytes());
            }

            fileWriter.close();
            LOGGER.debug("Application was processed successfully");


    }

    /**
     * Method provides natural ordered sorting by key incoming {@link Map}
     *
     * @param map {@link Map} which needs to be sorted
     * @return sorted {@link Map}
     */
    private Map<String, String> getSortedMap(Map<String, String> map) {
        return map.entrySet()
                .stream()
                .sorted(Map.Entry.comparingByKey())
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue,
                        (oldValue, newValue) -> oldValue, LinkedHashMap::new));
    }

   /**
    * Method provides walking along files tree and
    *
    * @param path {@link Path} which are start on
    */
   protected void walk(Path path) throws IOException {
      Files.walkFileTree(path, new FileVisitor<Path>() {
         @Override
         public FileVisitResult preVisitDirectory(Path directory, BasicFileAttributes attrs) throws IOException {
            return FileVisitResult.CONTINUE;
         }

         @Override
         public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
            String hash = hashProvider.getHash(fileReader.readFile(file));
            LOGGER.debug("Calculated hash for file: {}", file.toString());
            container.put(file.toString(), hash);
            return FileVisitResult.CONTINUE;
         }

         @Override
         public FileVisitResult visitFileFailed(Path file, IOException e) throws IOException {
            LOGGER.debug("Some problems were occurred during visiting a file {}:", file.toString(), e);
            throw e;
         }

         @Override
         public FileVisitResult postVisitDirectory(Path directory, IOException e) throws IOException {
            String hash = hashProvider.getHash(Arrays.stream(directory.toFile().listFiles())
               .sorted(Comparator.comparing(File::getName))
               .map(file -> container.get(file.toString()))
               .collect(Collectors.joining()).getBytes());
            LOGGER.debug("Calculated hash for directory: {}", directory.toString());
            container.put(directory.toString(), hash);
            return FileVisitResult.CONTINUE;
         }
      });
   }

   public static final class Builder {
      private HashProvider hashProvider;
      private Path inputPath;
      private FileReader fileReader;
      private FileWriter fileWriter;

      private Builder() {
      }

      public Builder setHashProvider(HashProvider hashProvider) {
         this.hashProvider = hashProvider;
         return this;
      }

      public Builder setInputPath(Path inputPath) {
         this.inputPath = inputPath;
         return this;
      }

      public Builder setFileReader(FileReader fileReader) {
         this.fileReader = fileReader;
         return this;
      }

      public Builder setFileWriter(FileWriter fileWriter) {
         this.fileWriter = fileWriter;
         return this;
      }

      public HashFilesService build() {
         return new HashFilesService(this);
      }
   }
}

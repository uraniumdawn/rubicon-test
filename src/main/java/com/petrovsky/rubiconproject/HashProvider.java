package com.petrovsky.rubiconproject;

/**
 * General interface provides a hash calculation of incoming bytes
 * <p>
 * Copyright © 2016 Rubicon Project, All rights reserved.
 */
interface HashProvider {

    String getHash(byte[] inputBytes);
}

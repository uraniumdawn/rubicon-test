package com.petrovsky.rubiconproject;

import org.junit.Before;
import org.junit.Test;

import java.io.FileNotFoundException;
import java.nio.file.Path;
import java.nio.file.Paths;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

public class FileReaderTest {
    private FileReader fileReader;
    private Path input;
    private String sample = "A-Nam no homero dictas percipitur, nec elitr impedit moderatius cu.";

    @Before
    public void setUp() throws Exception {
        fileReader = spy(new FileReader());
    }

    @Test
    public void readFile() throws Exception {
        input = Paths.get("src/test/resources/read_file_test.dat");
        byte[] bytes = fileReader.readFile(input);
        verify(fileReader, times(1)).readFile(input);
        assertArrayEquals(sample.getBytes(), bytes);
    }

    @Test(expected = FileNotFoundException.class)
    public void readNotExistedFile() throws Exception {
        input = Paths.get("src/test/resources/not_existed_file.dat");
        fileReader.readFile(input);
        verify(fileReader, times(1)).readFile(input);
    }
}
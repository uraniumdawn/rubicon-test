package com.petrovsky.rubiconproject;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.file.Path;

/**
 * Class provides read data from file
 * <p>
 * Copyright © 2016 Rubicon Project, All rights reserved.
 */
public class FileReader {
    private static final Logger LOGGER = LogManager.getLogger(FileReader.class);

    /**
     * Method provides reading from file
     * Byte buffer bound of Integer.MAX_VALUE
     *
     * @param path {@link Path} of file
     * @return read bytes
     */
    public byte[] readFile(Path path) throws IOException {
        try (RandomAccessFile inputFile = new RandomAccessFile(path.toFile(), "r")) {

            FileChannel fileChannel = inputFile.getChannel();
            ByteBuffer byteBuffer = ByteBuffer.wrap(new byte[(int) fileChannel.size()]);
            int readeByteCount = fileChannel.read(byteBuffer);
            LOGGER.debug("Read {} bytes from file {}", readeByteCount, path.toString());
            return byteBuffer.array();
        }
    }
}

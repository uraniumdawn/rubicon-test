Project description
============
The single solution of Rubicon project testing task

## Build and pass Tests
Application is built using [Apache Maven](http://maven.apache.org/)

to build application and pass test implicitly, in root directory of cloned project run:

    mvn clean package

to build application without tests, run:

    mvn -DskipTests clean package

to pass tests, run:

    mvn clean test

## Run
For execution application, run

    java jar rubicon-test.jar <path>
path - argument which is a directory of files which application starts calculate hash from.
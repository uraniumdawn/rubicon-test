package com.petrovsky.rubiconproject;

import java.io.IOException;
import java.nio.file.Paths;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Entry point for Application
 * <p>
 * Copyright © 2016 Rubicon Project, All rights reserved.
 */
public class Application {
    private static final Logger LOGGER = LogManager.getLogger(Application.class);

    public static void main(String[] argv) throws IOException {

        if (argv.length < 1) {
            LOGGER.debug("There is one argument expected: <path>");
            System.exit(1);
        }

        FileWriter fileWriter = new FileWriter(Paths.get("output/result.txt"));
        fileWriter.allocateBuffer(1024);

        HashFilesService hashFilesService = HashFilesService.newBuilder()
           .setInputPath(Paths.get(argv[0]))
           .setFileReader(new FileReader())
           .setFileWriter(fileWriter)
           .setHashProvider(new SHA512HashProvider())
           .build();

        hashFilesService.process();
    }
}

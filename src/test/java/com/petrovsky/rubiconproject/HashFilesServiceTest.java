package com.petrovsky.rubiconproject;

import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.Paths;

import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public class HashFilesServiceTest {
    private HashProvider hashProvider;
    private HashFilesService.Builder hashFilesServiceBuilder = HashFilesService.newBuilder();
    private HashFilesService hashFilesService;
    private Path output;
    private FileReader fileReader;
    private FileWriter fileWriter;

    @Before
    public void setUp() throws Exception {
        hashProvider = spy(new SHA512HashProvider());
        output = Paths.get("./hash_files_service_test_result.txt");
        fileReader = spy(new FileReader());
        fileWriter = spy(new FileWriter(output));
        fileWriter.allocateBuffer(1024);

        hashFilesServiceBuilder
           .setFileReader(fileReader)
           .setFileWriter(fileWriter)
           .setHashProvider(hashProvider);
    }

    @After
    public void tearDown() throws Exception {
        Files.deleteIfExists(output);
    }

    @Test
    public void processFilesTest() throws Exception {
        Path input = Paths.get("src/test/resources/process_files/input");
        Path sample = Paths.get("src/test/resources/process_files/sample/sample.txt");
        hashFilesService = spy(hashFilesServiceBuilder
           .setInputPath(input)
           .build());

        hashFilesService.process();

        verify(hashFilesService, times(1)).walk(input);
        verify(fileReader, times(5)).readFile(any(Path.class));
        verify(fileWriter, times(8)).write(any(byte[].class));
        verify(hashProvider, times(8)).getHash(any(byte[].class));
        assertTrue(FileUtils.contentEquals(output.toFile(), sample.toFile()));
    }

    @Test
    public void processIfInputPathIsFileTest() throws Exception {
        Path input = Paths.get("src/test/resources/process_if_input_path_is_file/fileA.dat");
        Path sample = Paths.get("src/test/resources/process_if_input_path_is_file/sample/sample.txt");
        hashFilesService = spy(hashFilesServiceBuilder
           .setInputPath(input)
           .build());

        hashFilesService.process();

        verify(hashFilesService, times(1)).walk(input);
        verify(fileReader, times(1)).readFile(any(Path.class));
        verify(fileWriter, times(1)).write(any(byte[].class));
        verify(hashProvider, times(1)).getHash(any(byte[].class));
        assertTrue(FileUtils.contentEquals(output.toFile(), sample.toFile()));
    }

    @Test
    public void processSameContentFilesTest() throws Exception {
        Path input = Paths.get("src/test/resources/process_same_content_files/input");
        Path sample = Paths.get("src/test/resources/process_same_content_files/sample/sample.txt");
        hashFilesService = spy(hashFilesServiceBuilder
           .setInputPath(input)
           .build());

        hashFilesService.process();

        verify(hashFilesService, times(1)).walk(input);
        verify(fileReader, times(5)).readFile(any(Path.class));
        verify(fileWriter, times(8)).write(any(byte[].class));
        verify(hashProvider, times(8)).getHash(any(byte[].class));
        assertTrue(FileUtils.contentEquals(output.toFile(), sample.toFile()));
    }

    @Test
    public void processEmptyFileTest() throws Exception {
        Path input = Paths.get("src/test/resources/process_empty_files/input");
        Path sample = Paths.get("src/test/resources/process_empty_files/sample/sample.txt");
        hashFilesService = spy(hashFilesServiceBuilder
           .setInputPath(input)
           .build());

        hashFilesService.process();

        verify(hashFilesService, times(1)).walk(input);
        verify(fileReader, times(1)).readFile(any(Path.class));
        verify(fileWriter, times(2)).write(any(byte[].class));
        verify(hashProvider, times(2)).getHash(any(byte[].class));
        assertTrue(FileUtils.contentEquals(output.toFile(), sample.toFile()));
    }

    // Test is ignored because git doesn't track empty directories but for success test's passing
    // it is necessary presence empty directory by path src/test/resources/process_empty_directory/input
    @Ignore
    @Test
    public void processEmptyDirectoryTest() throws Exception {
        Path input = Paths.get("src/test/resources/process_empty_directory/input");
        Path sample = Paths.get("src/test/resources/process_empty_directory/sample/sample.txt");
        hashFilesService = spy(hashFilesServiceBuilder
                .setInputPath(input)
                .build());

        hashFilesService.process();

        verify(hashFilesService, times(1)).walk(input);
        verify(fileReader, never()).readFile(any(Path.class));
        verify(fileWriter, times(1)).write(any(byte[].class));
        verify(hashProvider, times(1)).getHash(any(byte[].class));
        assertTrue(FileUtils.contentEquals(output.toFile(), sample.toFile()));
    }

    @Test(expected = NoSuchFileException.class)
    public void processNotExistedDirectoryTest() throws Exception {
        Path input = Paths.get("src/test/resources/process_not_existed_directory/input");
        hashFilesService = spy(hashFilesServiceBuilder
                .setInputPath(input)
                .build());

        hashFilesService.process();
    }
}
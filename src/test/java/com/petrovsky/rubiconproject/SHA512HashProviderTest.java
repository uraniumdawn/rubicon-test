package com.petrovsky.rubiconproject;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class SHA512HashProviderTest {
    private HashProvider hashProvider;
    private static final byte[] bytes = "mock content".getBytes();
    private static final String HASH = "977f6cb835553ffbcb0d3206dafce8431381974c5098016178586d3a0c2643d26ff5d323c5a3032acc5b4133db8c2ae2763da32dfb0f56a2646a5e6ae70e75a6";

    @Before
    public void setUp() throws Exception {
        hashProvider = new SHA512HashProvider();
    }

    @Test
    public void getHash() throws Exception {
        assertEquals(hashProvider.getHash(bytes), HASH);
    }
}
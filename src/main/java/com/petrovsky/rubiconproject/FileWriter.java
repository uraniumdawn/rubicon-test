package com.petrovsky.rubiconproject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Path;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Class provides write data to file
 * <p>
 * Copyright © 2016 Rubicon Project, All rights reserved.
 */
public class FileWriter {
    private static final Logger LOGGER = LogManager.getLogger(FileWriter.class);

    private FileChannel fileChannel;
    private Path output;
    private ByteBuffer byteBuffer;
    private RandomAccessFile randomAccessFile;

    public FileWriter(Path path) {
        output = path;
    }

    /**
     * Method provides creation output file.
     * @throws FileAlreadyExistsException if a file already exists
     */
    public void createFile() throws IOException {
        Files.createDirectories(output.getParent());
        Files.createFile(output);
        LOGGER.debug("Destination file was created.");
    }

    /**
     * Method provides creation {@link RandomAccessFile},
     * open and assign it file channel.
     * @throws FileNotFoundException if a file does not exist
     */
    public void openChannel() throws FileNotFoundException {
        randomAccessFile = new RandomAccessFile(output.toFile(), "rw");
        fileChannel = randomAccessFile.getChannel();
        LOGGER.debug("File channel was opened.");
    }

    /**
     * Method provides allocating a {@link ByteBuffer} with specified capacity.
     * @param capacity specified capacity
     */
    public void allocateBuffer(int capacity) {
        byteBuffer = ByteBuffer.allocate(capacity);
        LOGGER.debug("Byte buffer was created.");
    }

    /**
     * Method provides writing to file
     *
     * @param bytes data to writing
     */
    public void write(byte[] bytes) throws IOException {
        byteBuffer.clear();
        byteBuffer.put(bytes);
        byteBuffer.flip();

        LOGGER.debug("Writing to file {}", output.toString());
        while (byteBuffer.hasRemaining()) {
            fileChannel.write(byteBuffer);
        }
    }

    /**
     * Method provides closing {{@link #randomAccessFile}}
     */
    public void close() throws IOException {
        fileChannel.close();
        randomAccessFile.close();
        LOGGER.debug("File channel was closed.");
    }
}
package com.petrovsky.rubiconproject;

import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

public class FileWriterTest {
    private FileWriter fileWriter;
    private Path output;
    private String inputData = "A-Nam no homero dictas percipitur, nec elitr impedit moderatius cu.";

    @Before
    public void setUp() throws Exception {
        output = Paths.get("./file_writer_test_results.txt");
        fileWriter = spy(new FileWriter(output));
    }

    @After
    public void tearDown() throws Exception {
        Files.deleteIfExists(output);
    }

    @Test
    public void writeFileTest() throws Exception {
        Path sample = Paths.get("src/test/resources/write_file_sample.txt");

        fileWriter.createFile();
        fileWriter.openChannel();
        fileWriter.allocateBuffer(1024);
        fileWriter.write(inputData.getBytes());
        fileWriter.close();
        verify(fileWriter, times(1)).createFile();
        verify(fileWriter, times(1)).openChannel();
        verify(fileWriter, times(1)).write(any(byte[].class));
        verify(fileWriter, times(1)).close();
        assertTrue(FileUtils.contentEquals(output.toFile(), sample.toFile()));
    }

    @Test(expected = FileAlreadyExistsException.class)
    public void writeAlreadyExistedFileTest() throws Exception {
        fileWriter.createFile();
        fileWriter.createFile();
    }

}